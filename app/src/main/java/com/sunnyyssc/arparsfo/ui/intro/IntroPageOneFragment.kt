package com.sunnyyssc.arparsfo.ui.intro

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.sunnyyssc.arparsfo.databinding.IntroOneFragmentBinding

class IntroPageOneFragment : Fragment() {

    private var introOneFragmentBinding: IntroOneFragmentBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        introOneFragmentBinding = IntroOneFragmentBinding.inflate(inflater, container, false)
        return introOneFragmentBinding?.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        introOneFragmentBinding = null
    }
}
