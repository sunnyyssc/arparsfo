package com.sunnyyssc.arparsfo.data.model

import androidx.annotation.Keep
import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json

@Keep
data class CountryData(
    @Json(name = "data") val data: Map<String, CountryItem>
)

@Keep
@Entity(tableName = "country")
data class CountryItem(
    @PrimaryKey @ColumnInfo(name = "country_code") @Json(name = "iso_alpha2") val countryCode: String,
    @ColumnInfo(name = "country_name") @Json(name = "name") val countryName: String,
    @ColumnInfo(name = "country_continent") @Json(name = "continent") val countryContinent: String,
    @Embedded @Json(name = "advisory") val advisory: CountryAdvisory
)

@Keep
data class CountryAdvisory(
    @ColumnInfo(name = "risk_score") @Json(name = "score") val countryScore: Double,
    @ColumnInfo(name = "advisory_sources_active") @Json(name = "sources_active") val activeSources: Int,
    @ColumnInfo(name = "description") @Json(name = "message") val countryMessage: String,
    @ColumnInfo(name = "updated_at") @Json(name = "updated") val updatedAt: String,
    @Json(name = "source") val source: String
)

/**
 * This data class represents the result from
 * [com.sunnyyssc.arparsfo.data.dao.CountryDao.readCountryInfo].
 */
data class CountryRoomResult(
    @ColumnInfo(name = "country_name") val countryName: String,
    @ColumnInfo(name = "risk_score") val countryScore: Double,
    @ColumnInfo(name = "description") val countryMessage: String,
    @ColumnInfo(name = "updated_at") val updatedAt: String
)
