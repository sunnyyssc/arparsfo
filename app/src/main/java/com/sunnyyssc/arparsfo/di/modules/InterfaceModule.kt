package com.sunnyyssc.arparsfo.di.modules

import com.sunnyyssc.arparsfo.service.LastLocationFinder
import com.sunnyyssc.arparsfo.ui.main.FinderImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent

@Module
@InstallIn(ActivityComponent::class)
abstract class InterfaceModule {

    @Binds
    abstract fun bindLastLocationFinder(finderImp: FinderImpl): LastLocationFinder.LastLocationFinderInterface
}
