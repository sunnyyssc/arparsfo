package com.sunnyyssc.arparsfo.ui.general

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.sunnyyssc.arparsfo.R
import com.sunnyyssc.arparsfo.databinding.AboutFragmentBinding

class AboutFragment : Fragment() {

    private var aboutFragmentBinding: AboutFragmentBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        aboutFragmentBinding = AboutFragmentBinding.inflate(inflater, container, false)
        return aboutFragmentBinding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        aboutFragmentBinding?.apply {
            libraryTextView.setOnClickListener(libraryTextViewListener)
            emailTextView.setOnClickListener(emailTextViewViewListener)
            privacyPolicyTextView.setOnClickListener(privacyPolicyTextViewListener)
            sourceCodeTextView.setOnClickListener(sourceCodeTextViewListener)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        aboutFragmentBinding = null
    }

    private val libraryTextViewListener = View.OnClickListener {
        findNavController().navigate(R.id.action_aboutFragment_to_libraryFragment)
    }

    private val emailTextViewViewListener = View.OnClickListener {
        context?.let {
            openUrlFromStringSource(R.string.author_email_address)
        }
    }

    private val privacyPolicyTextViewListener = View.OnClickListener {
        context?.let {
            openUrlFromStringSource(R.string.app_privacy_policy)
        }
    }

    private val sourceCodeTextViewListener = View.OnClickListener {
        context?.let {
            openUrlFromStringSource(R.string.source_code_location)
        }
    }

    private fun openUrlFromStringSource(stringId: Int) {
        context?.let {
            try {
                startActivity(
                    Intent(Intent.ACTION_VIEW).apply {
                        data = Uri.parse(it.getString(stringId))
                    }
                )
            } catch (e: ActivityNotFoundException) {
                Toast.makeText(
                    it,
                    "No application is found that can handle this action",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }
}
