package com.sunnyyssc.arparsfo.ui.main

import androidx.annotation.Keep
import com.google.ar.core.AugmentedImageDatabase
import com.google.ar.core.Config
import com.google.ar.core.Session
import com.google.ar.sceneform.ux.ArFragment
import timber.log.Timber
import java.io.IOException

@Keep
class ArImageFragment : ArFragment() {

    private val imgdbTag = "IMGDB"

    /**
     *  Override method that does nothing to stop
     *  Sceneform from handling the camera permission.
     */
    override fun requestDangerousPermissions() {
    }

    override fun getSessionConfiguration(session: Session?): Config {
        planeDiscoveryController.hide()
        planeDiscoveryController.setInstructionView(null)
        arSceneView.planeRenderer.isEnabled = false
        val config = Config(session).apply {
            focusMode = Config.FocusMode.AUTO
            updateMode = Config.UpdateMode.LATEST_CAMERA_IMAGE
        }
        session?.configure(config)
        arSceneView.setupSession(session)

        if (setupAugmentedImageDatabase(session, config)) {
            Timber.tag(imgdbTag).i("Setup successful")
        } else {
            Timber.tag(imgdbTag).e("Failed to set up img db")
        }

        return config
    }

    private fun setupAugmentedImageDatabase(session: Session?, config: Config): Boolean {
        val augmentedImageDb: AugmentedImageDatabase

        try {
            val inputStream = activity?.assets?.open("pp.imgdb")
            augmentedImageDb = AugmentedImageDatabase.deserialize(session, inputStream)
        } catch (e: IOException) {
            Timber.tag(imgdbTag).e(e)
            return false
        }

        config.augmentedImageDatabase = augmentedImageDb
        return true
    }
}
