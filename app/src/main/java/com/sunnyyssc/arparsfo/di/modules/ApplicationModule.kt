package com.sunnyyssc.arparsfo.di.modules

import android.content.Context
import android.net.ConnectivityManager
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.sunnyyssc.arparsfo.data.datastore.DataStoreManager
import com.sunnyyssc.arparsfo.retrofit.RetrofitClient
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ApplicationModule {

    @Singleton
    @Provides
    fun provideRetrofit() = RetrofitClient.retrofitService

    @Singleton
    @Provides
    fun provideFirebaseCrashlytics() = FirebaseCrashlytics.getInstance()

    @Singleton
    @Provides
    fun provideConnectivityManager(@ApplicationContext appContext: Context) =
        appContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    @Singleton
    @Provides
    fun provideDataStoreManager(@ApplicationContext appContext: Context) =
        DataStoreManager(appContext)
}
