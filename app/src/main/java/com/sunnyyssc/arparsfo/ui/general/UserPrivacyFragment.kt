package com.sunnyyssc.arparsfo.ui.general

import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.sunnyyssc.arparsfo.R
import com.sunnyyssc.arparsfo.databinding.UserPrivacyFragmentBinding
import com.sunnyyssc.arparsfo.helper.getSpannedTextFromHtml

class UserPrivacyFragment : Fragment() {

    private var userPrivacyFragmentBinding: UserPrivacyFragmentBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        userPrivacyFragmentBinding = UserPrivacyFragmentBinding.inflate(inflater, container, false)
        return userPrivacyFragmentBinding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val userPrivacyTextView = userPrivacyFragmentBinding?.userPrivacyTextView
        clickableLinkFromStringSource(getString(R.string.arcore_notice_msg), userPrivacyTextView)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        userPrivacyFragmentBinding = null
    }

    private fun clickableLinkFromStringSource(string: String, textview: TextView?) {
        textview?.text = getSpannedTextFromHtml(string)
        textview?.movementMethod = LinkMovementMethod.getInstance()
    }
}
