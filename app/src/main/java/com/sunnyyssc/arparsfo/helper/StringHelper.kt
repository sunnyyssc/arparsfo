package com.sunnyyssc.arparsfo.helper

import androidx.core.text.HtmlCompat

val getSpannedTextFromHtml =
    { str: String -> HtmlCompat.fromHtml(str, HtmlCompat.FROM_HTML_MODE_COMPACT) }
