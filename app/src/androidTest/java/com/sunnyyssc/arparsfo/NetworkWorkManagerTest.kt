package com.sunnyyssc.arparsfo

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.util.Log
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.work.Configuration
import androidx.work.Constraints
import androidx.work.NetworkType
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkInfo
import androidx.work.WorkManager
import androidx.work.Worker
import androidx.work.WorkerParameters
import androidx.work.impl.utils.SynchronousExecutor
import androidx.work.testing.WorkManagerTestInitHelper
import androidx.work.workDataOf
import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert.assertEquals
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.util.concurrent.TimeUnit

@RunWith(AndroidJUnit4::class)
class NetworkWorkManagerTest {

    private lateinit var mContext: Context
    private lateinit var connectivityManager: ConnectivityManager

    @Before
    fun setup() {
        mContext = InstrumentationRegistry.getInstrumentation().targetContext

        connectivityManager =
            mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val config = Configuration.Builder()
            // Set log level to Log.DEBUG to make it easier to debug
            .setMinimumLoggingLevel(Log.DEBUG)
            // Use a SynchronousExecutor here to make it easier to write tests
            .setExecutor(SynchronousExecutor())
            .build()

        // Initialize WorkManager for instrumentation tests.
        WorkManagerTestInitHelper.initializeTestWorkManager(mContext, config)
    }

    @Test
    @Throws(Exception::class)
    fun testConstraints() {
        // Define input data
        val input = workDataOf("INPUT_1" to 1, "INPUT_2" to 2)

        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.NOT_REQUIRED)
            .build()

        // Create request
        val request = OneTimeWorkRequestBuilder<InputTestWorker>()
            .setInputData(input)
            .setConstraints(constraints)
            .build()

        val workManager = WorkManager.getInstance(mContext)
        val testDriver = WorkManagerTestInitHelper.getTestDriver(mContext)
        // Enqueue and wait for result.
        workManager.enqueue(request).result.get()
        testDriver?.setAllConstraintsMet(request.id)
        // Get WorkInfo and outputData
        val workInfo = workManager.getWorkInfoById(request.id).get()
        val outputData = workInfo.outputData
        // Assert
        assertThat(workInfo.state, `is`(WorkInfo.State.SUCCEEDED))
        assertEquals(outputData, input)
    }

    @Test
    @Throws(Exception::class)
    fun testPeriodicWorkRequest() {
        // Create request
        val request = PeriodicWorkRequestBuilder<NetworkStateTestWorker>(15, TimeUnit.MINUTES)
            .build()

        val workManager = WorkManager.getInstance(mContext)
        val testDriver = WorkManagerTestInitHelper.getTestDriver(mContext)
        // Enqueue and wait for result.
        workManager.enqueue(request).result.get()
        // Tells the testing framework the period delay is met
        testDriver?.setPeriodDelayMet(request.id)
        // Get WorkInfo and outputData
        val workInfo = workManager.getWorkInfoById(request.id).get()
        // Assert
        assertThat(workInfo.state, `is`(WorkInfo.State.ENQUEUED))
    }

    @Test
    @Throws(Exception::class)
    fun testNetwork() {
        val expectedBoolean = true
        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.NOT_REQUIRED)
            .build()

        // Create request
        val request = OneTimeWorkRequestBuilder<NetworkStateTestWorker>()
            .setConstraints(constraints)
            .build()

        val workManager = WorkManager.getInstance(mContext)
        val testDriver = WorkManagerTestInitHelper.getTestDriver(mContext)
        // Enqueue and wait for result.
        workManager.enqueue(request).result.get()
        testDriver?.setAllConstraintsMet(request.id)
        // Get WorkInfo and outputData
        val workInfo = workManager.getWorkInfoById(request.id).get()
        val outputBoolean = workInfo.outputData.keyValueMap["result"]
        // Assert
        assertThat(workInfo.state, `is`(WorkInfo.State.SUCCEEDED))
        assertEquals(expectedBoolean, outputBoolean)
    }

    class InputTestWorker(context: Context, parameters: WorkerParameters) :
        Worker(context, parameters) {
        override fun doWork(): Result {
            return when (inputData.size()) {
                0 -> Result.failure()
                else -> Result.success(inputData)
            }
        }
    }

    class NetworkStateTestWorker(appContext: Context, workerParams: WorkerParameters) :
        Worker(appContext, workerParams) {
        override fun doWork(): Result {
            val workResult = fun(): Boolean {
                val connectivityManager =
                    this.applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val capabilities =
                    connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
                return when {
                    capabilities == null -> false
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                    else -> false
                }
            }

            val output = workDataOf("result" to workResult())
            return Result.success(output)
        }
    }
}
