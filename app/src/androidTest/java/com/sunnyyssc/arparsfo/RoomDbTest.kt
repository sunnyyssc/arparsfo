package com.sunnyyssc.arparsfo

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.sunnyyssc.arparsfo.data.dao.CountryDao
import com.sunnyyssc.arparsfo.data.database.CountryDatabase
import com.sunnyyssc.arparsfo.data.model.CountryAdvisory
import com.sunnyyssc.arparsfo.data.model.CountryItem
import com.sunnyyssc.arparsfo.data.model.CountryRoomResult
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class RoomDbTest {

    private lateinit var testDb: CountryDatabase
    private lateinit var countryDao: CountryDao
    private lateinit var firstCountryAdvisory: CountryAdvisory
    private lateinit var secondCountryAdvisory: CountryAdvisory
    private lateinit var firstCountryItem: CountryItem
    private lateinit var secondCountryItem: CountryItem
    private lateinit var firstRoomResult: CountryRoomResult
    private lateinit var secondRoomResult: CountryRoomResult
    private lateinit var countryItemList: List<CountryItem>

    @Before
    fun iniTest() {
        val appContext = ApplicationProvider.getApplicationContext<Context>()

        testDb = Room.inMemoryDatabaseBuilder(
            appContext, CountryDatabase::class.java
        ).build()

        countryDao = testDb.countryDao()

        firstCountryAdvisory = CountryAdvisory(1.12, 3, "Test Message 1", "2020-02-15 07:25:06", "https://www.travel-advisory.info/canada")
        secondCountryAdvisory = CountryAdvisory(1.20, 4, "Test Message 2", "2020-02-15 07:25:06", "https://www.travel-advisory.info/australia")

        firstCountryItem = CountryItem("CA", "Canada", "NA", firstCountryAdvisory)
        secondCountryItem = CountryItem("AU", "Australia", "OCE", secondCountryAdvisory)

        firstRoomResult = CountryRoomResult("Canada", 1.12, "Test Message 1", "2020-02-15 07:25:06")
        secondRoomResult = CountryRoomResult("Australia", 1.20, "Test Message 2", "2020-02-15 07:25:06")

        countryItemList = arrayListOf<CountryItem>().apply {
            this.add(firstCountryItem)
            this.add(secondCountryItem)
        }

        CoroutineScope(Dispatchers.IO).launch {
            countryDao.insertAllCountry(countryItemList)
        }
    }

    @After
    @Throws(IOException::class)
    fun closeDB() {
        testDb.close()
    }

    @Test
    @Throws(Exception::class)
    suspend fun isReadCountryListEqual() {
        val firstResult = countryDao.readCountryInfo("CA")
        val secondResult = countryDao.readCountryInfo("AU")

        assertEquals(firstRoomResult, firstResult)
        assertEquals(secondRoomResult, secondResult)
    }

    @Test
    @Throws(Exception::class)
    suspend fun isReadCountryListNotEqual() {
        val firstResult = countryDao.readCountryInfo("CA")
        val secondResult = countryDao.readCountryInfo("AU")

        assertNotEquals(secondRoomResult, firstResult)
        assertNotEquals(firstRoomResult, secondResult)
    }
}
