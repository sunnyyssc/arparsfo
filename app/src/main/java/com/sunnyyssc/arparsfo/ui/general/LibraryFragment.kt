package com.sunnyyssc.arparsfo.ui.general

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sunnyyssc.arparsfo.adapter.LibraryAdapter
import com.sunnyyssc.arparsfo.databinding.LibraryFragmentBinding
import com.sunnyyssc.arparsfo.helper.getLibraryMapListFromJsonFile

class LibraryFragment : Fragment() {

    private var libraryFragmentBinding: LibraryFragmentBinding? = null
    private lateinit var recyclerView: RecyclerView
    private lateinit var libraryMapList: List<Map<String, String>>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        libraryFragmentBinding = LibraryFragmentBinding.inflate(inflater, container, false)
        return libraryFragmentBinding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        libraryMapList = getLibraryMapListFromJsonFile(activity?.applicationContext as Context)
        val linearLayoutManager = LinearLayoutManager(context)
        val libraryAdapter = LibraryAdapter(libraryMapList)
        recyclerView = libraryFragmentBinding?.libraryRecyclerView?.apply {
            layoutManager = linearLayoutManager
            adapter = libraryAdapter
        } as RecyclerView
    }

    override fun onDestroyView() {
        super.onDestroyView()
        libraryFragmentBinding = null
    }
}
