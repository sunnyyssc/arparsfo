package com.sunnyyssc.arparsfo.repository

import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.sunnyyssc.arparsfo.data.database.CountryDatabase
import com.sunnyyssc.arparsfo.data.model.CountryData
import com.sunnyyssc.arparsfo.data.model.CountryItem
import com.sunnyyssc.arparsfo.retrofit.RetrofitService
import dagger.hilt.android.scopes.ViewModelScoped
import io.reactivex.subjects.PublishSubject
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.util.Locale
import javax.inject.Inject
import kotlin.collections.HashMap

@ViewModelScoped
class CountryRepository @Inject constructor(
    private val apiService: RetrofitService,
    private val countryDb: CountryDatabase,
    private val firebaseCrashlytics: FirebaseCrashlytics
) {

    /** A Rx subject of country data. */
    private val countryInfoSubject: PublishSubject<Map<String, String>> = PublishSubject.create()
    /** A Rx subject of retrofit response. */
    private val retrofitResponseSubject: PublishSubject<String> = PublishSubject.create()

    /** Get information of a [countryCode]. */
    suspend fun fetchCountryInfo(countryCode: String) {
        val result = searchRoomCountryInfo(countryCode)

        if (result != null) {
            updateCountryInfoSubject(result)
            updateResponseSubject("Showing: ${result["country_name"]}")
        } else {
            val apiCall = apiService.getCountryInfo(countryCode)

            apiCall.enqueue(object : Callback<CountryData> {
                override fun onFailure(call: Call<CountryData>, t: Throwable) {
                    Timber.d(t)
                    firebaseCrashlytics.recordException(t)
                    updateResponseSubject("Failed to get country info")
                }

                override fun onResponse(call: Call<CountryData>, response: Response<CountryData>) {
                    if (response.isSuccessful) {
                        response.body()?.let {
                            val countryItem = it.data[countryCode]
                            if (countryItem != null) {
                                val infoMap = HashMap<String, String>()
                                infoMap["country_code"] = countryItem.countryCode.lowercase(Locale.ROOT)
                                infoMap["country_name"] = countryItem.countryName
                                infoMap["country_score"] = countryItem.advisory.countryScore.toString()
                                infoMap["country_message"] = countryItem.advisory.countryMessage
                                updateCountryInfoSubject(infoMap)
                                updateResponseSubject("Showing: ${infoMap["country_name"]}")

                                //  Add the country in the DB
                                CoroutineScope(Dispatchers.IO).launch {
                                    countryDb.countryDao().insertCountry(countryItem)
                                }
                            }
                        }
                    } else {
                        updateResponseSubject("Country info not found, try download again.(Internet required)")
                        Timber.d(response.toString())
                        firebaseCrashlytics.log(response.toString())
                    }
                }
            })
        }
    }

    fun fetchAllCountryInfoForRoom() {
        val apiCall = apiService.getAllCountryInfo()
        updateResponseSubject("Downloading all country info...")
        apiCall.enqueue(object : Callback<CountryData> {
            override fun onFailure(call: Call<CountryData>, t: Throwable) {
                Timber.d(t)
                firebaseCrashlytics.recordException(t)
                updateResponseSubject("Download Failed")
            }

            override fun onResponse(call: Call<CountryData>, response: Response<CountryData>) {
                if (response.isSuccessful) {
                    response.body()?.let {
                        val countryItemList = arrayListOf<CountryItem>()

                        for (countryItem in it.data.values) {
                            countryItemList.add(countryItem)
                        }
                        CoroutineScope(Dispatchers.IO).launch {
                            countryDb.countryDao().insertAllCountry(countryItemList)
                            // Switch back to the main thread to update response.
                            withContext(Dispatchers.Main) {
                                updateResponseSubject("Download Finished")
                            }
                        }
                    }
                } else {
                    Timber.d(response.toString())
                    firebaseCrashlytics.log(response.toString())
                }
            }
        })
    }

    private suspend fun searchRoomCountryInfo(countryCode: String): Map<String, String>? {
        val result = countryDb.countryDao().readCountryInfo(countryCode)

        @Suppress("SENSELESS_COMPARISON")
        return if (result == null) {
            null
        } else {
            val resultMap = HashMap<String, String>()
            resultMap["country_name"] = result.countryName
            resultMap["country_score"] = result.countryScore.toString()
            resultMap["country_message"] = result.countryMessage
            resultMap
        }
    }

    /** Calls Rx observer onNext method to update observed country information data. */
    private fun updateCountryInfoSubject(countryInfoMap: Map<String, String>) {
        countryInfoSubject.onNext(countryInfoMap)
    }

    /** Calls Rx observer onNext method to update observed country information data. */
    private fun updateResponseSubject(response: String) {
        retrofitResponseSubject.onNext(response)
    }

    /** Provides a Rx subject of countryInfo as an observable. */
    fun getCountryInfoObservableSubject() = countryInfoSubject

    /** Provides a Rx subject of response as an observable. */
    fun getResponseObservableSubject() = retrofitResponseSubject
}
