package com.sunnyyssc.arparsfo.data.datastore

import android.content.Context
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.preferencesDataStore
import com.sunnyyssc.arparsfo.R
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

private val Context.prefDataStore by preferencesDataStore("app_pref")

class DataStoreManager constructor(private val appContext: Context) {

    private val boolPrefKey =
        booleanPreferencesKey(appContext.getString(R.string.app_preferences_file_key))

    fun readFromPrefDataStore(): Flow<Boolean> {
        return appContext.prefDataStore.data.map { it[boolPrefKey] ?: true }
    }

    suspend fun writeToPrefDataStore(firstTime: Boolean) {
        appContext.prefDataStore.edit { it[boolPrefKey] = firstTime }
    }
}
