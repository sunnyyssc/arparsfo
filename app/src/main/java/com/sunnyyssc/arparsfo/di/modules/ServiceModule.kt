package com.sunnyyssc.arparsfo.di.modules

import android.content.Context
import androidx.room.Room
import com.google.android.gms.location.LocationServices
import com.sunnyyssc.arparsfo.data.database.CountryDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ServiceModule {

    @Provides
    @Singleton
    fun provideFusedLocationProviderClient(@ApplicationContext context: Context) =
        LocationServices.getFusedLocationProviderClient(context)

    @Provides
    @Singleton
    fun provideCountryDatabase(@ApplicationContext context: Context): CountryDatabase {
        return Room.databaseBuilder(
            context.applicationContext,
            CountryDatabase::class.java,
            "country-db"
        ).build()
    }
}
