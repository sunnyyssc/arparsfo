package com.sunnyyssc.arparsfo.retrofit

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

/**
 * A singleton of Retrofit client.
 *
 * This creates a client that uses RetrofitService interface.
 */
object RetrofitClient {
    private val moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
    val retrofitService: RetrofitService = Retrofit.Builder()
        .baseUrl("https://www.travel-advisory.info/")
        .client(OkHttpClient())
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .build()
        .create(RetrofitService::class.java)
}
