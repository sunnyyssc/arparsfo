package com.sunnyyssc.arparsfo

import com.sunnyyssc.arparsfo.helper.convertCountryNameToCode
import org.junit.Assert
import org.junit.Test

class CountryHelperTest {
    @Test
    fun isCovertCountryNameToCodeWithSpaceCorrect() {
        val result = convertCountryNameToCode("New Zealand")
        Assert.assertEquals("NZ", result)
    }

    @Test
    fun isCovertCountryNameToCodeWithoutSpaceCorrect() {
        val result = convertCountryNameToCode("Australia")
        Assert.assertEquals("AU", result)
    }
}
