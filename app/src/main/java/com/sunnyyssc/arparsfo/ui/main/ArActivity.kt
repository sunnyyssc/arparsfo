package com.sunnyyssc.arparsfo.ui.main

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.ResultReceiver
import android.provider.Settings
import android.text.format.DateFormat
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.activity.result.IntentSenderRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.core.view.GravityCompat
import androidx.fragment.app.FragmentManager
import androidx.navigation.NavController
import androidx.navigation.NavGraph
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.get
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkInfo
import androidx.work.WorkManager
import coil.load
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.google.ar.core.Anchor
import com.google.ar.core.AugmentedImage
import com.google.ar.core.TrackingState
import com.google.ar.sceneform.AnchorNode
import com.google.ar.sceneform.FrameTime
import com.google.ar.sceneform.Node
import com.google.ar.sceneform.math.Quaternion
import com.google.ar.sceneform.math.Vector3
import com.google.ar.sceneform.rendering.Renderable
import com.google.ar.sceneform.rendering.ViewRenderable
import com.google.ar.sceneform.ux.ArFragment
import com.sunnyyssc.arparsfo.R
import com.sunnyyssc.arparsfo.databinding.ArActivityBinding
import com.sunnyyssc.arparsfo.databinding.ArLayoutBinding
import com.sunnyyssc.arparsfo.helper.convertCountryNameToCode
import com.sunnyyssc.arparsfo.helper.getSpannedTextFromHtml
import com.sunnyyssc.arparsfo.service.Constants
import com.sunnyyssc.arparsfo.service.FetchAddressJobIntentService
import com.sunnyyssc.arparsfo.service.LastLocationFinder
import com.sunnyyssc.arparsfo.ui.search.SearchActivity
import com.sunnyyssc.arparsfo.viewmodel.MainViewModel
import com.sunnyyssc.arparsfo.worker.NETWORK_RESULT_KEY
import com.sunnyyssc.arparsfo.worker.NETWORK_TASK_UNIQUE_NAME
import com.sunnyyssc.arparsfo.worker.NetworkStateWorker
import com.sunnyyssc.arparsfo.worker.NetworkWorker
import com.sunnyyssc.arparsfo.worker.constraints
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import java.util.Locale
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@AndroidEntryPoint
class ArActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    @Inject
    lateinit var lastLocationFinder: LastLocationFinder

    @Inject
    lateinit var networkWorker: NetworkWorker
    lateinit var countryCodeResultReceiver: CountryCodeResultReceiver

    private lateinit var arActivityBinding: ArActivityBinding
    private lateinit var arLayoutBinding: ArLayoutBinding
    private lateinit var fragmentManger: FragmentManager
    private lateinit var arImageFragment: ArImageFragment

    /** The view model for main activity. */
    private val mainViewModel: MainViewModel by viewModels()
    private lateinit var builder: AlertDialog.Builder
    private lateinit var snackbar: Snackbar
    private var objectInScene = false
    private var isNetworkFound = false
    private var isRequestLocationDialogShown = false
    private var isSearchCountry = false
    private val allPermissionsArray = arrayOf(
        Manifest.permission.CAMERA,
        Manifest.permission.ACCESS_FINE_LOCATION
    )

    /** A long value of system time of back button press. */
    private var backButtonPressedTime = 0L

    /** A toast widget for back button. */
    private val backButtonToast by lazy {
        Toast.makeText(
            this,
            "Press again to exit",
            Toast.LENGTH_SHORT
        )
    }
    private lateinit var navHostFragment: NavHostFragment
    private lateinit var navController: NavController

    companion object {
        private const val TrackingState_TAG: String = "TrackingState"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arActivityBinding = ArActivityBinding.inflate(layoutInflater).apply {
            setContentView(root)
            fragmentManger = supportFragmentManager
            navHostFragment = fragmentManger.findFragmentById(R.id.arFragmentHost) as NavHostFragment
            navController = navHostFragment.navController

            val appBarConfig = AppBarConfiguration(navController.graph, arActivityDrawerLayout)
            arActivityToolbar.setupWithNavController(navController, appBarConfig)
            arActivityNavView.setupWithNavController(navController)

            arActivityAppBarLayout.bringToFront()
            arActivityToolbar.title = ""
            arActivityNavView.setNavigationItemSelectedListener(this@ArActivity)

            refreshFab.setOnClickListener(refreshFabListener)
        }

        showRefreshFab(false)

        builder = AlertDialog.Builder(this)

        arLayoutBinding = ArLayoutBinding.inflate(layoutInflater).apply {
            countryNameTextView.text = getString(R.string.pending)
            countryScoreTextView.text = getString(R.string.pending)
            countryMessageTextView.text = getString(R.string.pending)
            updatedAtTextView.text = getString(R.string.pending)
        }

        isNetworkFound = networkWorker.isNetworkConnected()
        if (!isNetworkFound) {
            arActivityBinding.arActivityNavView.menu.clear()
            arActivityBinding.arActivityNavView.inflateMenu(R.menu.offline_nav_drawer)
            showSnackbarMessage(
                getString(R.string.snackbar_not_connected),
                Snackbar.LENGTH_INDEFINITE,
                searchButtonListener
            )
        }
        startNetworkWorker()
        initLiveDataObservers()
    }

    override fun onStart() {
        super.onStart()
        checkPermissions()
    }

    override fun onResume() {
        super.onResume()
        if (!::arImageFragment.isInitialized) {
            arImageFragment = navHostFragment.childFragmentManager.primaryNavigationFragment as ArImageFragment
            arImageFragment.arSceneView.scene.addOnUpdateListener(this::onUpdateFrame)
        }
        if (isNetworkFound && !isRequestLocationDialogShown) checkLocationSettings()
        if (!::countryCodeResultReceiver.isInitialized) countryCodeResultReceiver =
            CountryCodeResultReceiver(Handler(Looper.getMainLooper()))
    }

    override fun onPause() {
        super.onPause()
        if (::arImageFragment.isInitialized) arImageFragment.arSceneView.scene.removeOnUpdateListener(this::onUpdateFrame)
    }

    override fun onBackPressed() {
        if (arActivityBinding.arActivityDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            arActivityBinding.arActivityDrawerLayout.closeDrawer(GravityCompat.START)
            return
        } else {
            if (backButtonPressedTime + 2000 > System.currentTimeMillis()) {
                backButtonToast.cancel()
                super.onBackPressed()
            } else {
                backButtonToast.show()
            }
        }
        backButtonPressedTime = System.currentTimeMillis()
    }

    //  region Menu
    // Handle navigation view item clicks here.
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_search -> startSearchActivity()
            R.id.nav_download -> mainViewModel.downloadAllCountryInfoForRoom()
            R.id.nav_user_privacy -> startGeneralActivity("UserPrivacyFragment")
            R.id.nav_about -> startGeneralActivity("AboutFragment")
        }

        arActivityBinding.arActivityDrawerLayout.closeDrawer(GravityCompat.START)
        return true
    }
    //  endregion

    //  region Start activities
    private fun startSearchActivity() {
        // Use the old fashion way to get result from another activity
        val intent = Intent(this, SearchActivity::class.java)
        startSearchActivityForResult.launch(intent)
    }

    private fun startGeneralActivity(fragmentName: String) {
        // Create a bundle for fragment_name
        val bundle = bundleOf(APP_BAR_ACTIVITY_FRAGMENT_NAME to fragmentName)
        // Get destination of the nested graph and cast it back to nav graph
        val nested = navController.graph[R.id.nav_item_nav_graph] as NavGraph
        // Set the nested graph start destination
        nested.startDestination = R.id.generalActivity
        // Navigate to the nested graph with argument
        navController.navigate(R.id.nav_to_nav_item_graph, bundle)
    }

    private val startSearchActivityForResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        isSearchCountry = true
        val selectedCountry = it.data?.getStringExtra("selected_country")
        selectedCountry ?: return@registerForActivityResult
        val result = convertCountryNameToCode(selectedCountry)
        mainViewModel.requestCountryInfo(result)
    }

    private val startLocationSettingForResult = registerForActivityResult(ActivityResultContracts.StartIntentSenderForResult()) {
        if (it.resultCode == Activity.RESULT_OK) {
            getLastCountryCode()
        } else if (it.resultCode == Activity.RESULT_CANCELED) {
            showSnackbarMessage(
                getString(R.string.snackbar_location_settings_cancel),
                Snackbar.LENGTH_INDEFINITE,
                searchButtonListener
            )
        }
    }
    //  endregion

    //  region Internet Connection check
    /**
     * Starts Network worker via a WorkManager for checking the network state every 15 minutes.
     */
    private fun startNetworkWorker() {
        val periodicWorkRequest =
            PeriodicWorkRequestBuilder<NetworkStateWorker>(15, TimeUnit.MINUTES).setConstraints(
                constraints
            ).build()
        WorkManager.getInstance(applicationContext).enqueueUniquePeriodicWork(
            NETWORK_TASK_UNIQUE_NAME,
            ExistingPeriodicWorkPolicy.KEEP,
            periodicWorkRequest
        )
        WorkManager.getInstance(this).getWorkInfoByIdLiveData(periodicWorkRequest.id)
            .observe(
                this,
                { workInfo ->
                    if (workInfo != null && workInfo.state == WorkInfo.State.SUCCEEDED) {
                        val output = workInfo.outputData.keyValueMap[NETWORK_RESULT_KEY] as Boolean
                        checkInternetConnection(output)
                    }
                }
            )
    }

    private fun checkInternetConnection(connected: Boolean) {
        isNetworkFound = connected
        with(arActivityBinding.arActivityNavView) {
            if (isNetworkFound) {
                menu.clear()
                inflateMenu(R.menu.online_nav_drawer)
                if (::snackbar.isInitialized) snackbar.dismiss()
            } else {
                menu.clear()
                inflateMenu(R.menu.offline_nav_drawer)
                showSnackbarMessage(
                    getString(R.string.snackbar_not_connected),
                    Snackbar.LENGTH_INDEFINITE,
                    searchButtonListener
                )
            }
        }
        invalidateOptionsMenu()
    }
    //  endregion

    //  region Sceneform
    @Suppress("UNUSED_PARAMETER", "WHEN_ENUM_CAN_BE_NULL_IN_JAVA")
    private fun onUpdateFrame(frameTime: FrameTime?) {
        val frame = arImageFragment.arSceneView.arFrame
        frame?.let {
            val augmentedImages = it.getUpdatedTrackables(AugmentedImage::class.java)
            for (arImg in augmentedImages) {
                Timber.tag(TrackingState_TAG)
                    .i("${arImg.trackingState} and ${arImg.trackingMethod}")
                when (arImg.trackingState) {
                    TrackingState.TRACKING -> {
                        when (arImg.name) {
                            "nzl" -> {
                                if (!objectInScene) {
                                    renderTextView(
                                        arImageFragment,
                                        arImg.createAnchor(arImg.centerPose)
                                    )
                                    objectInScene = true
                                }
                            }
                        }
                    }
                    TrackingState.PAUSED -> Timber.tag(TrackingState_TAG).i("PAUSED")
                    TrackingState.STOPPED -> {}
                }
            }
        }
    }

    private fun renderTextView(arFragment: ArFragment, anchor: Anchor) {
        ViewRenderable.builder()
            .setView(arFragment.context, arLayoutBinding.root)
            .build()
            .thenAccept { viewRenderable ->
                addNodeToScene(
                    arFragment,
                    anchor,
                    viewRenderable.also { it.isShadowCaster = false }
                )
            }
            .exceptionally { throwable ->
                Toast.makeText(this, "Error:" + throwable.message, Toast.LENGTH_LONG).show()
                null
            }
    }

    //  Attach the renderable to a node and set itself to the Anchor node
    private fun addNodeToScene(arFragment: ArFragment, anchor: Anchor, renderable: Renderable) {
        val anchorNode = AnchorNode(anchor)
        val node = Node()
        node.localPosition = Vector3(0f, 0f, 0.01f)
        node.localRotation = Quaternion.axisAngle(Vector3(-1f, 0f, 0f), 90f)
        node.localScale = Vector3(0.1f, 0.1f, 0.1f)
        node.renderable = renderable
        node.setParent(anchorNode)
        arFragment.arSceneView.scene.addChild(anchorNode)
    }
    // endregion

    // region LiveData
    private fun initLiveDataObservers() {
        if (!mainViewModel.countryLiveData.hasObservers()) observeCountryData()
        if (!mainViewModel.responseLiveData.hasObservers()) observeResponseString()
    }

    private fun observeCountryData() {
        mainViewModel.countryLiveData.observe(
            this,
            { countryInfoMap ->
                with(arLayoutBinding) {
                    countryFlagImageView.load("https://www.countryflags.io/${countryInfoMap["country_code"]}/flat/64.png") {
                        allowHardware(false)
                        placeholder(R.drawable.ic_flag_64)
                        error(R.drawable.ic_flag_64)
                    }
                    countryNameTextView.text = countryInfoMap["country_name"]

                    val score = countryInfoMap["country_score"]?.toDoubleOrNull()
                    if (score != null) {
                        with(riskScoreCardView) {
                            when {
                                score <= 2.5 -> setCardBackgroundColor(ContextCompat.getColor(this@ArActivity, R.color.warningGreen))
                                score in 2.5..3.5 -> setCardBackgroundColor(ContextCompat.getColor(this@ArActivity, R.color.warningBlue))
                                score in 3.5..4.5 -> setCardBackgroundColor(ContextCompat.getColor(this@ArActivity, R.color.warningYellow))
                                score >= 4.5 -> setCardBackgroundColor(ContextCompat.getColor(this@ArActivity, R.color.warningRed))
                            }
                        }
                    }

                    countryScoreTextView.text = getSpannedTextFromHtml(getString(R.string.country_score, countryInfoMap["country_score"]))

                    countryMessageTextView.text = getSpannedTextFromHtml(
                        getString(
                            R.string.country_message,
                            if (countryInfoMap["country_message"].isNullOrBlank()) "Not Available" else countryInfoMap["country_message"]
                        )
                    )

                    val dateTimePattern = DateFormat.getBestDateTimePattern(Locale.ROOT, "ddMMMMyyyy hhmma")
                    updatedAtTextView.text = getSpannedTextFromHtml(getString(R.string.country_last_updated, DateFormat.format(dateTimePattern, System.currentTimeMillis())))
                }
            }
        )
    }

    private fun observeResponseString() {
        mainViewModel.responseLiveData.observe(
            this,
            { response ->
                showSnackbarMessage(response, Snackbar.LENGTH_LONG)
            }
        )
    }
    // endregion

    //  region Service
    fun startGeoCoderService(receiver: ResultReceiver, latLong: Location) {
        if (isSearchCountry) {
            isSearchCountry = false
            showRefreshFab(true)
            return
        } else {
            showSnackbarMessage(
                getString(R.string.country_code, "Fetching..."),
                Snackbar.LENGTH_INDEFINITE
            )
            val intent = Intent(this, FetchAddressJobIntentService::class.java).apply {
                putExtra(Constants.RECEIVER, receiver)
                putExtra(Constants.LOCATION_DATA_EXTRA, latLong)
            }
            FetchAddressJobIntentService.startServiceJob(this, intent)
        }
    }

    inner class CountryCodeResultReceiver(handler: Handler) : ResultReceiver(handler) {
        override fun onReceiveResult(resultCode: Int, resultData: Bundle?) {
            if (resultCode == Constants.SUCCESS_RESULT) {
                resultData?.getString(Constants.RESULT_DATA_KEY)?.also {
                    showSnackbarMessage(getString(R.string.country_code, it), Snackbar.LENGTH_LONG)
                    mainViewModel.requestCountryInfo(it)
                }
            }
        }
    }
    // endregion

    // region Permissions
    private val requestPermissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { results ->
            results.forEach {
                if (!it.value) {
                    when (it.key) {
                        Manifest.permission.CAMERA -> {
                            if (shouldShowRequestPermissionRationale(it.key)) {
                                showExplanationMessageDialog(
                                    getString(R.string.camera_permission_title),
                                    getText(R.string.camera_permission_message).toString()
                                )
                            } else {
                                directToSystemPermissionSettingsDialog(
                                    getString(R.string.permission_required),
                                    getString(R.string.permission_not_granted)
                                )
                            }
                        }

                        Manifest.permission.ACCESS_FINE_LOCATION -> {
                            if (shouldShowRequestPermissionRationale(it.key)) {
                                showExplanationMessageDialog(
                                    getString(R.string.location_permission_title),
                                    getText(R.string.location_permission_message).toString()
                                )
                            }
                        }
                    }
                }
            }
        }

    private fun checkPermissions() {
        val isCameraPermissionGranted =
            ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
        val isLocationPermissionGranted =
            ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)

        if (isCameraPermissionGranted != PackageManager.PERMISSION_GRANTED ||
            isLocationPermissionGranted != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissionLauncher.launch(allPermissionsArray)
        }
    }

    private fun showExplanationMessageDialog(title: String, message: String) {
        builder.setTitle(title).setMessage(message)
            .setPositiveButton(getString(R.string.ok)) { _, _ ->
                requestPermissionLauncher.launch(allPermissionsArray)
            }.setCancelable(false).show().setCanceledOnTouchOutside(false)
    }

    private fun directToSystemPermissionSettingsDialog(title: String, message: String) {
        builder.setTitle(title).setMessage(message)
            .setPositiveButton(getString(R.string.go_to_settings)) { _, _ ->
                val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                intent.data = Uri.parse("package:$packageName")
                startActivity(intent)
            }
            .setNegativeButton(getString(R.string.exit)) { _, _ ->
                finish()
            }.setCancelable(false).show().setCanceledOnTouchOutside(false)
    }
    // endregion

    // region Location
    private fun checkLocationSettings() {
        lastLocationFinder.requestLocationSettings()
    }

    fun getLastCountryCode() {
        lastLocationFinder.getLastLatLongResult()
    }

    fun requestLocationSettingsDialog(
        title: String,
        message: String,
        ex: ResolvableApiException
    ) {
        isRequestLocationDialogShown = true // Change to true to stop the dialog showing again
        builder.setTitle(title).setMessage(message)
            .setPositiveButton(getString(R.string.ok)) { _, _ ->
                try {
                    startLocationSettingForResult.launch(IntentSenderRequest.Builder(ex.resolution).build())
                } catch (sendEx: IntentSender.SendIntentException) {
                    // Ignore the error.
                }
            }.setCancelable(false).show().setCanceledOnTouchOutside(false)
    }
    // endregion

    //  region UI components
    fun showSnackbarMessage(
        string: String,
        duration: Int,
        listener: View.OnClickListener? = null
    ) {
        val view = arActivityBinding.arActivityCoordinatorLayout
        snackbar = if (listener != null) {
            Snackbar.make(view, string, duration)
                .setAction(R.string.search_button, listener).also { it.show() }
        } else {
            Snackbar.make(view, string, duration).also { it.show() }
        }
    }

    private fun showRefreshFab(show: Boolean) {
        with(arActivityBinding.refreshFab) { if (show) show() else hide() }
    }
    //  endregion

    //  region Button Listener
    val searchButtonListener = View.OnClickListener { startSearchActivity() }
    private val refreshFabListener = View.OnClickListener {
        getLastCountryCode()
        showRefreshFab(false)
    }
    //  endregion
}

class FinderImpl @Inject constructor(
    private val activity: Activity
) : LastLocationFinder.LastLocationFinderInterface {
    override fun onLatLongResultUpdate(latLongResult: Location?) {
        if (activity is ArActivity) {
            with(activity) {
                if (latLongResult != null) {
                    lastLocationFinder.stopLocationUpdate()
                    startGeoCoderService(countryCodeResultReceiver, latLongResult)
                } else {
                    showSnackbarMessage(
                        getString(R.string.snackbar_not_connected),
                        Snackbar.LENGTH_INDEFINITE,
                        searchButtonListener
                    )
                }
            }
        }
    }

    override fun onCheckLocationSettingResult(ex: ResolvableApiException?) {
        if (activity is ArActivity) {
            with(activity) {
                if (ex != null) {
                    requestLocationSettingsDialog(
                        getString(R.string.location_setting_missing),
                        getString(R.string.location_setting_required),
                        ex
                    )
                } else {
                    getLastCountryCode()
                }
            }
        }
    }
}
