package com.sunnyyssc.arparsfo.ui.intro

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.sunnyyssc.arparsfo.databinding.IntroThreeFragmentBinding

class IntroPageThreeFragment : Fragment() {

    private var introThreeFragmentBinding: IntroThreeFragmentBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        introThreeFragmentBinding = IntroThreeFragmentBinding.inflate(inflater, container, false)
        return introThreeFragmentBinding?.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        introThreeFragmentBinding = null
    }
}
