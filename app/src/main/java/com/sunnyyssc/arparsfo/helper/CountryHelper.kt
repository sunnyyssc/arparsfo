package com.sunnyyssc.arparsfo.helper

import java.util.Locale
import kotlin.collections.ArrayList

val allCountry = { list: ArrayList<String> ->
    Locale.getISOCountries().forEach {
        list.add(Locale("", it).displayCountry)
    }
    list
}

val convertCountryNameToCode = { countryName: String ->
    Locale.getISOCountries().find {
        Locale("", it).displayCountry == countryName
    } as String
}
