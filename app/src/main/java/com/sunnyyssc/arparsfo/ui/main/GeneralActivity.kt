package com.sunnyyssc.arparsfo.ui.main

import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.fragment.app.FragmentManager
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.navigation.NavigationView
import com.sunnyyssc.arparsfo.R
import com.sunnyyssc.arparsfo.databinding.GeneralActivityBinding

const val APP_BAR_ACTIVITY_FRAGMENT_NAME = "FRAGMENT_NAME"

class GeneralActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var generalActivityBinding: GeneralActivityBinding
    private lateinit var fragmentManger: FragmentManager
    private lateinit var navHostFragment: NavHostFragment
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            window.decorView.apply {
                @Suppress("DEPRECATION")
                systemUiVisibility =
                    View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or View.SYSTEM_UI_FLAG_FULLSCREEN or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
            }
        } else {
            with(WindowInsetsControllerCompat(window, window.decorView)) {
                systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
                hide(WindowInsetsCompat.Type.statusBars() or WindowInsetsCompat.Type.navigationBars())
            }
        }

        generalActivityBinding = GeneralActivityBinding.inflate(layoutInflater).apply {
            setContentView(root)
        }

        fragmentManger = supportFragmentManager
        navHostFragment =
            fragmentManger.findFragmentById(R.id.generalActivityFragmentHost) as NavHostFragment
        navController = navHostFragment.navController
        // Inflate the graph
        val navInflater = navController.navInflater
        val navGraph = navInflater.inflate(R.navigation.app_bar_activity_nav_graph)
        // Check argument passed from ArActivity
        val extras = intent.extras
        if (extras != null) {
            when (extras.get(APP_BAR_ACTIVITY_FRAGMENT_NAME)) {
                "UserPrivacyFragment" -> navGraph.startDestination = R.id.userPrivacyFragment
                "AboutFragment" -> navGraph.startDestination = R.id.aboutFragment
            }
        }
        // Assign graph
        navController.graph = navGraph
        val appBarConfig =
            AppBarConfiguration(
                navController.graph,
                generalActivityBinding.generalActivityDrawerLayout
            )
        generalActivityBinding.generalActivityToolbar.setupWithNavController(
            navController,
            appBarConfig
        )
        generalActivityBinding.generalActivityNavView.setupWithNavController(navController)
        generalActivityBinding.generalActivityNavView.setNavigationItemSelectedListener(this)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_home -> finish()
            R.id.nav_user_privacy -> navigateToDestination(R.id.userPrivacyFragment)
            R.id.nav_about -> navigateToDestination(R.id.aboutFragment)
        }

        generalActivityBinding.generalActivityDrawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun navigateToDestination(destinationId: Int) {
        val currentDestinationId = navController.currentDestination?.id
        if (currentDestinationId != null && currentDestinationId != destinationId) {
            navController.navigate(destinationId)
        }
    }
}
