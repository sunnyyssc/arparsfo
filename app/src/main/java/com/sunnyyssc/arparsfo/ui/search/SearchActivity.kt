package com.sunnyyssc.arparsfo.ui.search

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sunnyyssc.arparsfo.R
import com.sunnyyssc.arparsfo.adapter.CountryAdapter
import com.sunnyyssc.arparsfo.databinding.SearchActivityBinding
import com.sunnyyssc.arparsfo.helper.allCountry

class SearchActivity : AppCompatActivity(), CountryAdapter.OnItemClickListener {

    private lateinit var searchActivityBinding: SearchActivityBinding
    private lateinit var recyclerView: RecyclerView
    private lateinit var countryAdapter: CountryAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        searchActivityBinding = SearchActivityBinding.inflate(layoutInflater).apply {
            setContentView(root)
            setSupportActionBar(searchToolbar)
        }
        setupSearch()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main_search, menu)
        // Get the SearchView and set the searchable configuration
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchActionItem =
            (menu?.findItem(R.id.action_search)?.actionView as SearchView).apply {
                // Assumes current activity is the searchable activity
                setSearchableInfo(searchManager.getSearchableInfo(componentName))
            }

        searchActionItem.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean = true

            override fun onQueryTextChange(newText: String?): Boolean {
                countryAdapter.filter.filter(newText)
                return false
            }
        })

        return true
    }

    private fun setupSearch() {
        val countryNameList = arrayListOf<String>().apply { allCountry(this).sort() }
        val recycleViewManager = LinearLayoutManager(this)
        val dividerItemDecoration = DividerItemDecoration(this, recycleViewManager.orientation)
        countryAdapter = CountryAdapter(countryNameList, this)
        recyclerView = searchActivityBinding.searchActivityContent.countryNameRecycleView.apply {
            layoutManager = recycleViewManager
            adapter = countryAdapter
            addItemDecoration(dividerItemDecoration)
        }
    }

    override fun onItemClicked(selectedCountryName: String) {
        val intent = Intent()
        intent.putExtra("selected_country", selectedCountryName)
        setResult(RESULT_FIRST_USER, intent)
        finish()
    }
}
