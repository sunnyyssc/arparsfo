package com.sunnyyssc.arparsfo.service

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.os.Looper
import android.os.ResultReceiver
import androidx.core.app.JobIntentService
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.sunnyyssc.arparsfo.R
import dagger.hilt.android.qualifiers.ActivityContext
import timber.log.Timber
import java.io.IOException
import java.util.Locale
import javax.inject.Inject

object Constants {
    const val SUCCESS_RESULT = 0
    const val FAILURE_RESULT = 1
    const val RECEIVER = "LOCATION_RECEIVER"
    const val RESULT_DATA_KEY = "RESULT_DATA_KEY"
    const val LOCATION_DATA_EXTRA = "LOCATION_DATA_EXTRA"
}

class LastLocationFinder @Inject constructor(
    lastLocationFinderInterface: LastLocationFinderInterface?,
    flc: FusedLocationProviderClient,
    @ActivityContext activityContext: Context
) {
    private val fusedLocationClient = flc
    private var mLastLocationFinderInterface = lastLocationFinderInterface
    private val context = activityContext
    private lateinit var locationCallback: LocationCallback
    private val locationRequest = LocationRequest.create().apply {
        priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        interval = 10000
    }

    fun getLastLatLongResult() {
        try {
            fusedLocationClient.lastLocation.addOnSuccessListener {
                if (it != null) {
                    mLastLocationFinderInterface?.onLatLongResultUpdate(it)
                } else {
                    //  If the last location is not found, then starts location update.
                    updateLocation()
                }
            }

            fusedLocationClient.lastLocation.addOnFailureListener {
                if (it is SecurityException) mLastLocationFinderInterface?.onLatLongResultUpdate(null)
            }
        } catch (ex: SecurityException) {
        }
    }

    private fun updateLocation() {
        if (!::locationCallback.isInitialized) {
            locationCallback = object : LocationCallback() {
                override fun onLocationResult(result: LocationResult?) {
                    if (result == null) return
                    for (location in result.locations) {
                        mLastLocationFinderInterface?.onLatLongResultUpdate(location)
                    }
                }
            }
        }

        try {
            fusedLocationClient.requestLocationUpdates(
                locationRequest,
                locationCallback,
                Looper.getMainLooper()
            )
        } catch (ex: SecurityException) {
        }
    }

    fun stopLocationUpdate() {
        if (::locationCallback.isInitialized)
            fusedLocationClient.removeLocationUpdates(locationCallback)
    }

    /* Check if location setting is enabled. */
    fun requestLocationSettings() {
        val locationSettingsRequestBuilder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        val settingClient = LocationServices.getSettingsClient(context)
        val task = settingClient.checkLocationSettings(locationSettingsRequestBuilder.build())

        task.addOnSuccessListener { response ->
            if (response?.locationSettingsStates?.isLocationPresent == true) {
                mLastLocationFinderInterface?.onCheckLocationSettingResult(null)
            }
        }

        task.addOnFailureListener { ex ->
            if (ex is ResolvableApiException) {
                mLastLocationFinderInterface?.onCheckLocationSettingResult(ex)
            }
        }
    }

    interface LastLocationFinderInterface {
        fun onLatLongResultUpdate(latLongResult: Location?)
        fun onCheckLocationSettingResult(ex: ResolvableApiException?)
    }
}

class FetchAddressJobIntentService : JobIntentService() {
    private var receiver: ResultReceiver? = null

    @Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    @SuppressLint("TimberExceptionLogging")
    override fun onHandleWork(intent: Intent) {
        val geoCoder = Geocoder(this, Locale.getDefault())
        var errorMessage = ""
        // Get the location passed to this service through an extra.
        receiver = intent.getParcelableExtra(Constants.RECEIVER)
        val location = intent.getParcelableExtra<Location>(Constants.LOCATION_DATA_EXTRA)
        var addresses: List<Address> = emptyList()

        try {
            location?.let {
                addresses = geoCoder.getFromLocation(location.latitude, location.longitude, 1)
            }
        } catch (ioException: IOException) {
            // Catch network or other I/O problems.
            errorMessage = getString(R.string.service_not_available)
            Timber.e(ioException, errorMessage)
        } catch (illegalArgumentException: IllegalArgumentException) {
            // Catch invalid latitude or longitude values.
            errorMessage = getString(R.string.invalid_lat_long_used)
            Timber.e(illegalArgumentException, errorMessage)
        }

        // Handle case where no address was found.
        if (addresses.isEmpty()) {
            if (errorMessage.isEmpty()) {
                errorMessage = getString(R.string.no_address_found)
            }
            deliverResultToReceiver(Constants.FAILURE_RESULT, errorMessage)
        } else {
            val address = addresses[0]
            // Fetch the address lines using getAddressLine,
            // join them, and send them to the thread.
            val addressFragments = with(address) {
                (0..maxAddressLineIndex).map {
                    countryCode
                }
            }
            deliverResultToReceiver(Constants.SUCCESS_RESULT, addressFragments.joinToString(separator = "\n"))
        }
    }

    private fun deliverResultToReceiver(resultCode: Int, message: String) {
        val bundle = Bundle().apply { putString(Constants.RESULT_DATA_KEY, message) }
        receiver?.send(resultCode, bundle)
    }

    companion object {
        private const val JOB_ID = 1

        fun startServiceJob(context: Context, intent: Intent) {
            enqueueWork(context, FetchAddressJobIntentService::class.java, JOB_ID, intent)
        }
    }
}
