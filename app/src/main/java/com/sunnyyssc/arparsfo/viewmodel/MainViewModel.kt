package com.sunnyyssc.arparsfo.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sunnyyssc.arparsfo.repository.CountryRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val countryRepository: CountryRepository,
    private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    /** Disposables of Rx which observe repository. */
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    /** Live data of country info. */
    val countryLiveData by lazy {
        MutableLiveData<Map<String, String>>().also { rxCountryObserver() }
    }

    /** Live data of retrofit response. */
    val responseLiveData by lazy {
        MutableLiveData<String>().also { rxResponseObserver() }
    }

    //region # Rx
    /** Observers country data changes. */
    private fun rxCountryObserver() {
        val countryObserverDisposable = countryRepository.getCountryInfoObservableSubject()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { countryLiveData.value = it },
                { e -> Timber.e(e) }
            )
        compositeDisposable.add(countryObserverDisposable)
    }

    /** Observers response changes. */
    private fun rxResponseObserver() {
        val responseObserverDisposable = countryRepository.getResponseObservableSubject()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { responseLiveData.value = it },
                { e -> Timber.e(e) }
            )
        compositeDisposable.add(responseObserverDisposable)
    }
    //endregion

    //region # Requests
    /** Requests a fetch of country info based on [countryCode]. */
    fun requestCountryInfo(countryCode: String) {
        viewModelScope.launch(Dispatchers.IO) {
            countryRepository.fetchCountryInfo(countryCode)
        }
    }

    fun downloadAllCountryInfoForRoom() {
        viewModelScope.launch(Dispatchers.IO) {
            countryRepository.fetchAllCountryInfoForRoom()
        }
    }
    //endregion

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}
