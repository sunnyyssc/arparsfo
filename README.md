# **Arparsfo**

Arparsfo is an open source android app that utilises Augmented Reality (AR) to show travel info via your passport front cover.

It currently shows travel advisory of a country which you locate in or have manually searched. The info are fetched from [Travel-Advisory.info](https://www.travel-advisory.info/).

See [supported passports](https://gitlab.com/sunnyyssc/arparsfo/-/wikis/Supported-Passports)

## Requirement

See [supported devices](https://developers.google.com/ar/discover/supported-devices#android_play) from ARCore to make sure if your android device can run the app.

## Build

You can build the project on [Android Studio](https://developer.android.com/studio/run) or from [command line](https://developer.android.com/studio/build/building-cmdline).

This app uses Firebase Crashlytics so a `google-services.json` file is needed in the `app` module to build the app successfully. For this reason, I have added a dummy file under `app/src/debug` to help build the app locally without creating a firebase project.

## License

This app is licensed under Apache License 2.0. See [LICENSE](https://gitlab.com/sunnyyssc/arparsfo/-/blob/master/LICENSE)

