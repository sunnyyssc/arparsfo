package com.sunnyyssc.arparsfo.retrofit

import com.sunnyyssc.arparsfo.data.model.CountryData
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * An interface of Retrofit API.
 *
 * This interface includes all Retrofit requests in the app.
 */

interface RetrofitService {
    @GET("api")
    fun getCountryInfo(@Query("countrycode") countryCode: String): Call<CountryData>

    @GET("api")
    fun getAllCountryInfo(): Call<CountryData>
}
