package com.sunnyyssc.arparsfo.ui.intro

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import com.sunnyyssc.arparsfo.R
import com.sunnyyssc.arparsfo.databinding.IntroActivityBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class IntroActivity : AppCompatActivity() {

    private lateinit var activityIntroBinding: IntroActivityBinding
    private lateinit var introFragment: IntroFragment
    private lateinit var fragmentManger: FragmentManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityIntroBinding = IntroActivityBinding.inflate(layoutInflater).apply {
            setContentView(root)
        }
        fragmentManger = supportFragmentManager
        showIntroFragment()
    }

    private fun showIntroFragment() {
        introFragment = IntroFragment()
        val transition = supportFragmentManager.beginTransaction()
        transition.add(R.id.introFragmentContainer, introFragment)
        transition.commit()
    }
}
