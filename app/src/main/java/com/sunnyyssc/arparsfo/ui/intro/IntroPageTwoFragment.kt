package com.sunnyyssc.arparsfo.ui.intro

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.sunnyyssc.arparsfo.databinding.IntroTwoFragmentBinding

class IntroPageTwoFragment : Fragment() {

    private var introTwoFragmentBinding: IntroTwoFragmentBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        introTwoFragmentBinding = IntroTwoFragmentBinding.inflate(inflater, container, false)
        return introTwoFragmentBinding?.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        introTwoFragmentBinding = null
    }
}
