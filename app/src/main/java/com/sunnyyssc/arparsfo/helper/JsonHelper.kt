package com.sunnyyssc.arparsfo.helper

import android.content.Context
import org.json.JSONArray

private val getLocalLibraryJsonFileString = { context: Context ->
    val jsonString = context.assets.open("library.json").bufferedReader().use { it.readText() }
    jsonString
}

val getLibraryMapListFromJsonFile = { context: Context ->
    val jsonArray = JSONArray(getLocalLibraryJsonFileString(context))
    val libraryMapList = arrayListOf<Map<String, String>>()
    for (index in 0 until jsonArray.length()) {
        val jsonObjectInArray = jsonArray.getJSONObject(index)
        val map = HashMap<String, String>().apply {
            put("name", jsonObjectInArray.getString("name"))
            put("owner", jsonObjectInArray.getString("owner"))
            put("url", jsonObjectInArray.getString("url"))
            put("license", jsonObjectInArray.getString("license"))
        }.toMap()
        libraryMapList.add(map)
    }
    libraryMapList.toList()
}
