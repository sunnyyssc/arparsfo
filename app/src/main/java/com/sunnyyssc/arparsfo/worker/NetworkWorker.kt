package com.sunnyyssc.arparsfo.worker

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import androidx.hilt.work.HiltWorker
import androidx.work.Constraints
import androidx.work.NetworkType
import androidx.work.Worker
import androidx.work.WorkerParameters
import androidx.work.workDataOf
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import javax.inject.Inject
import javax.inject.Singleton

const val NETWORK_RESULT_KEY = "NETWORK_RESULT_KEY"
const val NETWORK_TASK_UNIQUE_NAME = "Schedule location check"

val constraints =
    Constraints.Builder().setRequiredNetworkType(NetworkType.NOT_REQUIRED).build()

@Singleton
class NetworkWorker @Inject constructor(private val connectivityManager: ConnectivityManager) {
    @Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    fun isNetworkConnected(): Boolean {
        connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork).also {
            return when {
                it == null -> false
                it.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                it.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                it.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                else -> false
            }
        }
    }
}

@HiltWorker
class NetworkStateWorker @AssistedInject constructor(
    @Assisted appContext: Context,
    @Assisted workerParams: WorkerParameters,
    private val networkWorker: NetworkWorker
) :
    Worker(appContext, workerParams) {
    override fun doWork(): Result {
        val result = workDataOf(NETWORK_RESULT_KEY to networkWorker.isNetworkConnected())
        return Result.success(result)
    }
}
