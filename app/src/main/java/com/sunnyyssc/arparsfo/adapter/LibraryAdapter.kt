package com.sunnyyssc.arparsfo.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.sunnyyssc.arparsfo.R
import com.sunnyyssc.arparsfo.databinding.RecyclerviewLibraryItemBinding

class LibraryAdapter(private val libraryNameList: List<Map<String, String>>) :
    RecyclerView.Adapter<LibraryAdapter.LibraryViewHolder>() {

    class LibraryViewHolder(binding: RecyclerviewLibraryItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        private val libraryNameLicenseTextView = binding.libraryNameLicense
        private val libraryOwnerTextView = binding.libraryOwner
        private val libraryUrlTextView = binding.libraryUrl
        private val context = binding.root.context

        fun bind(libraryMapItem: Map<String, String>) {
            libraryNameLicenseTextView.text = context.getString(
                R.string.library_name_license_placeholder,
                libraryMapItem["name"],
                libraryMapItem["license"]
            )
            libraryOwnerTextView.text = libraryMapItem["owner"]
            libraryUrlTextView.text = libraryMapItem["url"]
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LibraryViewHolder {
        val binding = RecyclerviewLibraryItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )

        return LibraryViewHolder(binding)
    }

    override fun onBindViewHolder(holder: LibraryViewHolder, position: Int) {
        holder.bind(libraryNameList[position])
    }

    override fun getItemCount() = libraryNameList.size
}
