package com.sunnyyssc.arparsfo.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.sunnyyssc.arparsfo.data.dao.CountryDao
import com.sunnyyssc.arparsfo.data.model.CountryItem

@Database(entities = [CountryItem::class], version = 1)
abstract class CountryDatabase : RoomDatabase() {
    abstract fun countryDao(): CountryDao
}
