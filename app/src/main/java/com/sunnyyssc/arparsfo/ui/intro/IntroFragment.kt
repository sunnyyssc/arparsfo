package com.sunnyyssc.arparsfo.ui.intro

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayoutMediator
import com.sunnyyssc.arparsfo.data.datastore.DataStoreManager
import com.sunnyyssc.arparsfo.databinding.IntroFragmentBinding
import com.sunnyyssc.arparsfo.ui.main.ArActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.IOException
import javax.inject.Inject

@AndroidEntryPoint
class IntroFragment : Fragment() {

    @Inject
    lateinit var dataStoreManager: DataStoreManager

    private var introFragmentBinding: IntroFragmentBinding? = null
    private var currentPosition = 0
    private var button: Button? = null
    private var viewPager: ViewPager2? = null
    private val lastPosition = 2

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        introFragmentBinding = IntroFragmentBinding.inflate(inflater, container, false)
        return introFragmentBinding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewPager()
        button = introFragmentBinding?.introButton
        introButtonAction()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewPager?.unregisterOnPageChangeCallback(pageChangeCallBackObject)
        introFragmentBinding = null
    }

    private fun initViewPager() {
        val fragments =
            arrayListOf(IntroPageOneFragment(), IntroPageTwoFragment(), IntroPageThreeFragment())
        val viewPagerAdapter = ViewPagerAdapter(fragments, this)
        introFragmentBinding?.apply {
            viewPager = introFragmentViewPager
            viewPager?.adapter = viewPagerAdapter
            viewPager?.let {
                val tabLayout = introFragmentTabLayout
                TabLayoutMediator(tabLayout, it) { tab, position ->
                    tab.text = "$position"
                }.attach()
            }
            viewPager?.registerOnPageChangeCallback(pageChangeCallBackObject)
        }
    }

    class ViewPagerAdapter(
        private val mFragmentList: ArrayList<Fragment>,
        fragment: Fragment
    ) : FragmentStateAdapter(fragment) {
        override fun createFragment(position: Int): Fragment {
            return mFragmentList[position]
        }

        override fun getItemCount(): Int {
            return mFragmentList.size
        }
    }

    private val pageChangeCallBackObject = object : ViewPager2.OnPageChangeCallback() {
        override fun onPageSelected(position: Int) {
            currentPosition = position
            when (position) {
                lastPosition -> changeButtonText("Done")
                else -> changeButtonText("Next")
            }
        }
    }

    private fun introButtonAction() {
        button?.setOnClickListener {
            if (currentPosition == lastPosition) {
                lifecycleScope.launch(Dispatchers.IO) {
                    try {
                        dataStoreManager.writeToPrefDataStore(false)
                    } catch (ex: IOException) {
                    }
                    withContext(Dispatchers.Main) {
                        val intent = Intent(activity, ArActivity::class.java)
                        startActivity(intent)
                        activity?.finish()
                    }
                }
            } else {
                viewPager?.setCurrentItem(++currentPosition, true)
            }
        }
    }

    private fun changeButtonText(buttonText: String) {
        button?.text = buttonText
    }
}
