package com.sunnyyssc.arparsfo.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.sunnyyssc.arparsfo.R
import com.sunnyyssc.arparsfo.databinding.RecyclerviewCountryItemBinding
import java.util.Locale
import kotlin.collections.ArrayList

class CountryAdapter(
    private val countryDataSet: ArrayList<String>,
    private val onItemClickListener: OnItemClickListener
) :
    RecyclerView.Adapter<CountryAdapter.AdapterViewHolder>(), Filterable {

    private val dataSetCopy = arrayListOf<String>()

    init {
        dataSetCopy.addAll(countryDataSet)
    }

    class AdapterViewHolder(binding: RecyclerviewCountryItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        private val textView = binding.itemTextView

        fun bind(countryName: String, onItemClickListener: OnItemClickListener) {
            textView.text =
                itemView.context.getString(R.string.recycle_view_country_item, countryName)
            itemView.setOnClickListener {
                onItemClickListener.onItemClicked(countryName)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterViewHolder {
        val binding = RecyclerviewCountryItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )

        return AdapterViewHolder(binding)
    }

    override fun onBindViewHolder(holder: AdapterViewHolder, position: Int) {
        holder.bind(countryDataSet[position], onItemClickListener)
    }

    override fun getItemCount() = countryDataSet.size

    override fun getFilter(): Filter = textInputFilter

    private val textInputFilter = object : Filter() {
        override fun performFiltering(constraint: CharSequence?): FilterResults {
            val filteredList = arrayListOf<String>()
            filteredList.clear()
            if (constraint.isNullOrEmpty()) {
                filteredList.addAll(dataSetCopy)
            } else {
                val pattern = constraint.toString().lowercase(Locale.ENGLISH).trim()
                for (countryName in dataSetCopy) {
                    if (countryName.lowercase(Locale.ENGLISH).contains(pattern)) {
                        filteredList.add(countryName)
                    }
                }
            }

            return FilterResults().apply { values = filteredList }
        }

        @Suppress("UNCHECKED_CAST")
        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            countryDataSet.clear()
            countryDataSet.addAll(results?.values as ArrayList<String>)
            notifyDataSetChanged()
        }
    }

    interface OnItemClickListener {
        fun onItemClicked(selectedCountryName: String)
    }
}
