package com.sunnyyssc.arparsfo.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.sunnyyssc.arparsfo.data.model.CountryItem
import com.sunnyyssc.arparsfo.data.model.CountryRoomResult

@Dao
interface CountryDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllCountry(countryItemList: List<CountryItem>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCountry(countryItem: CountryItem)

    @Query("SELECT country_name, risk_score, description, updated_at FROM country WHERE country_code = :countryCode")
    suspend fun readCountryInfo(countryCode: String): CountryRoomResult
}
