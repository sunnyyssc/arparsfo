package com.sunnyyssc.arparsfo.ui

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.sunnyyssc.arparsfo.data.datastore.DataStoreManager
import com.sunnyyssc.arparsfo.ui.intro.IntroActivity
import com.sunnyyssc.arparsfo.ui.main.ArActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.IOException
import javax.inject.Inject

@AndroidEntryPoint
class SplashScreenActivity : AppCompatActivity() {

    @Inject lateinit var dataStoreManager: DataStoreManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope.launch(Dispatchers.IO) {
            val firstTimeLaunch = try {
                dataStoreManager.readFromPrefDataStore().first()
            } catch (ex: IOException) {
                true // Returns true to proceed to the app intro instead
            }
            withContext(Dispatchers.Main) {
                val intent = if (firstTimeLaunch) {
                    Intent(this@SplashScreenActivity, IntroActivity::class.java)
                } else {
                    Intent(this@SplashScreenActivity, ArActivity::class.java)
                }
                startActivity(intent)
                finish()
            }
        }
    }
}
